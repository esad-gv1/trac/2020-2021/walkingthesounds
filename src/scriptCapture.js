import * as Mobilizing from '@mobilizing/library';
import { TraiteurAcceleration } from "./tds";

export class Script2 {

    constructor() {

        this.accelProcessor = new TraiteurAcceleration(128 * 5, 100);

        console.log(this.accelProcessor);

        this.debugDiv = document.createElement("div");
        this.debugDiv.style.position = "fixed";
        this.debugDiv.style.color = "white";
        this.debugDiv.style.width = "300px";
        this.debugDiv.style.height = "200px";
        this.debugDiv.style.fontSize = "21px";
        //this.debugDiv.style.backgroundColor = "black";
        this.debugDiv.style.top = 0;
        this.debugDiv.style.left = 0;

        this.captureDiv = document.createElement("div");
        this.captureDiv.style.position = "fixed";
        this.captureDiv.style.color = "black";
        this.captureDiv.style.backgroundColor = "rgba(155,60,155,0.9)";
        this.captureDiv.style.width = "90vw";
        this.captureDiv.style.height = "90vh";
        this.captureDiv.style.fontSize = "21px";
        this.captureDiv.style.textAlign="center";

        this.btnCapture = document.createElement("div");
        this.btnCapture.style.position = "relative";
        this.btnCapture.style.display = "inline-block";
        this.btnCapture.style.height = "50px";
        this.btnCapture.style.width = "150px";
        this.btnCapture.style.top="100px";
        this.btnCapture.style.border = "solid white 2px";
        this.btnCapture.innerHTML = "boutonCapt";
        this.btnCapture.style.color="white";

        this.btnStop = document.createElement("div");
        this.btnStop.style.position = "relative";
        this.btnStop.style.display="inline-block";
        this.btnStop.style.height="50px";
        this.btnStop.style.width="150px";
        this.btnStop.style.border="white solid 2px";
        this.btnStop.style.color="white";
        this.btnStop.innerHTML="boutonStop";
        this.btnStop.style.top="100px";
        this.btnStop.style.left="100px";

        this.debugDiv2 = document.createElement("div");
        this.debugDiv2.style.display = "block";
        this.debugDiv2.style.position = "absolute";
        this.debugDiv2.style.height="300px";
        this.debugDiv2.style.width="300px";
        this.debugDiv2.style.border="white solid 2px";
        this.debugDiv2.style.color="white";
        this.debugDiv2.style.top="180px";
        this.debugDiv2.style.left="180px";

        this.captureDiv.appendChild(this.btnCapture);
    }

    // called just after the constructor
    preLoad(loader) {

    }

    // called when preloaded elements are loaded
    setup() {
        var arrayAccZ = [];
        var capteurAccZ = [];
        var arrayAccX = [];
        var capteurAccX = [];
        var arrayAccY = [];
        var capteurAccY = [];

        function captations(X,Y,Z){
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        document.body.appendChild(this.captureDiv);
        document.body.appendChild(this.debugDiv);
        document.body.appendChild(this.debugDiv2);
        document.body.appendChild(this.btnStop);
        console.log(this.captureDiv);

        this.orientation = new Mobilizing.input.Orientation();
        this.context.addComponent(this.orientation);
        this.orientation.setup();

        //!!!!!! Déclaration de ACCEL avec mobilizing !!!!!!!!!

        this.accel = new Mobilizing.input.Motion();
        this.accel = this.context.addComponent(this.accel);
        this.accel.setup();//set it up

        this.accel.events.on("acceleration", this.accelEvent.bind(this));

        //test blocage par loop

        var indexBlock=0;

        Isblocked();
        function Isblocked(){
            if(indexBlock<15000){
                console.log(indexBlock);
                add1Block();
            }
        }

        function add1Block(){
            setTimeout(function(){
                indexBlock++;
                Isblocked();
            },1000);
        }

        //captation : ajoute dans une array X Y et Z les dernières valeurs rendu par traiteurAcceleration

        var bufferAccZ=0;
        var blockageAcc=true;

        function fireAcc(){
            if(blockageAcc){
                console.log(bufferAccZ);
                //console.log(arrayAccZ);
                capteurAccZ.push(arrayAccZ[arrayAccZ.length-1]);
                capteurAccY.push(arrayAccY[arrayAccY.length-1]);
                capteurAccX.push(arrayAccX[arrayAccX.length-1]);
                console.log(capteurAccZ);
                console.log(capteurAccY);
                console.log(capteurAccX);
                refreshFire();
            }
        }

        var newCaptation;
        var jsonCaptation;
        function stopAccZ(){
            blockageAcc=false;
            fireAcc();
            newCaptation = new captations(capteurAccX,capteurAccY,capteurAccZ);
            jsonCaptation = JSON.stringify(newCaptation);
            showObj();
        }




        this.btnCapture.addEventListener("click",fireAcc);
        this.btnStop.addEventListener("click",stopAccZ);

        //setInterval(() => {
        //    this.debugDiv.innerHTML = this.accelProcessor.getAccelerationMoyenne("z");
        //},100);

        function refreshFire(){
            setTimeout(function(){
                bufferAccZ++;
                fireAcc();
            },500);
        }

        //Affiche dans le block principale les valeurs traitées "temps réel" renvoyer par les accéléromètres

        setTimeout(() => {
            setInterval(() => {
                this.debugDiv.innerHTML = this.accelProcessor.getAccelerationMoyenne("x").toFixed(2) + "<br>";
                arrayAccZ.push(this.accelProcessor.getAccelerationMoyenne("z"));
                this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("y").toFixed(2) + "<br>";
                arrayAccY.push(this.accelProcessor.getAccelerationMoyenne("y"));
                this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("z").toFixed(2);
                arrayAccX.push(this.accelProcessor.getAccelerationMoyenne("x"));
            }, 500);
        },3000);


        window.addEventListener("touchend", () => {
            this.activateSensors();
            console.log("toucher");
        });

        window.addEventListener("click", () => {
            this.activateSensors();
        });
    }

    activateSensors() {
        this.accel.on();//active it
        this.orientation.on();
    }

    accelEvent(acc) {
        this.accelProcessor.ajouterEchantillon(acc.x, acc.y, acc.z);
    }

    // refresh loop
    update() {

    }
}
