const path = require('path');
const RemovePlugin = require('remove-files-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = [
    {
        entry: './src/index.js',
        output: {
            filename: 'main.js',
            path: path.join(__dirname, 'dist', 'js'),
            libraryTarget: 'umd',
            library: 'main'
        },
        bail: true,
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env']
                            }
                        },
                    ]
                },
            ]
        },
        plugins: [
            new CopyPlugin({
                patterns: [{
                    from: path.resolve(__dirname, 'src/assets').replace(/\\/g, "/"),
                    to: path.resolve(__dirname, 'dist/assets').replace(/\\/g, "/"),
                    force: true,
                    noErrorOnMissing: true,
                }],
            }),
            new RemovePlugin({
                before: {
                    include: ['./dist/assets']
                },
                watch: {
                    include: ['./dist/assets']
                }
            }),
        ],
    },
]; // module exports
